using Cysharp.Threading.Tasks;
using DriVR.Helpers.Runtime;
using UnityEngine;
using XRInput.Runtime;
using DriVR.AIDriver.Runtime;
using DriVR.PlayersCar.Runtime;

public class ExperienceMain : ExperienceBase , IExperience
{
    #region Attributes
    private CarParameters carParameters;

    private DriVRDevice device;
    private VPPLinker linker;
    private GameObject car;
    private SpeedDashboardDisplay speedDashboardDisplay;
    private SpeedManager speedManager;

    private readonly float maxDisplaySpeedTarget = 50;
    private readonly float midDisplaySpeedTarget = 30;

    private bool linkControllerValueAndSpeedDashboardPicto = false;

    public bool startKeyPressed = false;
    public bool endKeyPressed = false;
    public bool escapeKeyPressed = false;


    #endregion
    public override async UniTaskVoid ExperienceLoop() //Deroule de l'experience
    {

    }
    public override void Configure() //Called on Awake
    {
        ctsUser = new System.Threading.CancellationTokenSource();

        device = FindObjectOfType<DriVRDevice>();

        //Conditions a surveiller pour changer d'etape
        experienceStartCondition = () => startKeyPressed == true;
        experienceEndCondition = () => endKeyPressed == true;
        experienceAbordCondition = () => escapeKeyPressed == true;

        //Action a realiser lorsque les etapes de l'experience sont terminees
        experienceStartAction += MessageStart;
        experienceStartAction += () => ConfigureCar();
        experienceEndAction = MessageEnd;
        experienceAbordAction = MessageAbord;

    }
    private void Update()
    {
        if(experienceStartCondition())
        {
            MapControllersToCarActions();
        }
        
    }   

    #region Monitor Experience steps
    public async UniTask<bool> MonitorStart()
    {
        await UniTask.WaitUntil(() => Input.GetKeyDown(KeyCode.S));
        startKeyPressed = true;
        return true;
    }

    public async UniTask<bool> MonitorEnd()
    {
        await UniTask.WaitUntil(() => Input.GetKeyDown(KeyCode.E));
        endKeyPressed = true;
        return true;
    }

    public async UniTask<bool> MonitorAbord()
    {
        await UniTask.WaitUntil(() => Input.GetKeyDown(KeyCode.Escape));
        escapeKeyPressed = true;
        return true;
    }

    #endregion

    #region Messages d'action
    public void MessageStart()
    {
        Debug.Log("Experience " + base.Data.ExperienceID + " is now started");
    }

    public void MessageEnd()
    {
        Debug.Log("Experience " + base.Data.ExperienceID + "  is ended");
    }

    public void MessageAbord()
    {
        Debug.Log("Experience " + base.Data.ExperienceID + "  is aborded");
    }

    #endregion

    #region Controller Mapping
    private void UpdateSpeedDashboardDisplayValues(float accelValue, float brakeValue)
    {
        if (linkControllerValueAndSpeedDashboardPicto)
            if (speedDashboardDisplay != null)
            {
                speedDashboardDisplay.SpeedSliderValue = accelValue;
                speedDashboardDisplay.BrakeSliderValue = brakeValue;

                speedDashboardDisplay.speedSlider.value = accelValue;
                speedDashboardDisplay.brakeSlider.value = brakeValue;

                if (accelValue <= 0.1f)
                {
                    speedDashboardDisplay.ShowSpeedImage(2);
                    return;
                }

                if (accelValue > 0.95f)
                {
                    speedDashboardDisplay.ShowSpeedImage(0);
                    return;
                }

                speedDashboardDisplay.ShowSpeedImage(1);
            }


    }
    
    private void MapControllersToCarActions()
    {
        if (Application.isEditor)
        {
            float a, b, c;
            float d, e, f;
            a = (Input.GetKey(KeyCode.Keypad3) == true ? 1 : 0) * 0.1f;
            b = (Input.GetKey(KeyCode.Keypad6) == true ? 1 : 0) * 0.5f;
            c = (Input.GetKey(KeyCode.Keypad9) == true ? 1 : 0) * 0.96f;
            d = (Input.GetKey(KeyCode.Keypad1) == true ? 1 : 0) * 0.1f;
            e = (Input.GetKey(KeyCode.Keypad4) == true ? 1 : 0) * 0.5f;
            f = (Input.GetKey(KeyCode.Keypad7) == true ? 1 : 0) * 0.96f;
            var totalAccel = Mathf.Clamp01(a + b + c);
            var totalBrake = Mathf.Clamp01(d + e + f);

            if (device != null)
            {
                device.controllerStateVariable.Value.right.Action = totalAccel;
                device.controllerStateVariable.Value.left.Action = totalBrake;
            }

            ComputeMaxAllowedSpeed();
            UpdateSpeedDashboardDisplayValues(device.controllerStateVariable.Value.right.Action, device.controllerStateVariable.Value.left.Action);

        }
        else
        {
            ComputeMaxAllowedSpeed();
            UpdateSpeedDashboardDisplayValues(device.controllerStateVariable.Value.right.Action, device.controllerStateVariable.Value.left.Action);

        }
    }

    private void ComputeMaxAllowedSpeed()
    {
        if (device != null)
        {
            if (device.controllerStateVariable.Value.right.Action > 0.95f)
            {
                linker.SendSpecialCommand("drive");
                carParameters.MaxAllowedSpeed = maxDisplaySpeedTarget;
            }
            else
            {
                if (device.controllerStateVariable.Value.right.Action > 0.1f)
                {
                    linker.SendSpecialCommand("drive");
                    carParameters.MaxAllowedSpeed = midDisplaySpeedTarget;
                }
                else
                {
                    linker.SendSpecialCommand("neutral");
                    carParameters.MaxAllowedSpeed = 0;
                }
            }
        }

    }
    #endregion

    #region Utility Functions
    public void ResetSpeedDashboardDisplay()
    {
        speedDashboardDisplay.speedSlider.value = 0;
        speedDashboardDisplay.brakeSlider.value = 0;
        speedDashboardDisplay.HideAllSpeedImages();
    }

    public async UniTask<bool> ConfigureCar()
    {
        linker = FindObjectOfType<VPPLinker>() as VPPLinker;
        car = linker.gameObject;
        speedDashboardDisplay = car.GetComponentInChildren<SpeedDashboardDisplay>();
        speedManager = car.GetComponent<SpeedManager>();
        carParameters = linker.CarParameters.Value;

        speedManager.speedAnalyzerType = SpeedAnalyzerType.Automatic;
        speedManager.speedControllerType = SpeedControllerType.Mixed;
        ResetSpeedDashboardDisplay();
        CarDriverConfigurator.SPEED_VR_ADAPT = 1.18f;
        return true;
    }
    #endregion
}
